# This is an example project that shows how to use Review Apps

This is a very basic project that has one static page, but it shows how to use
the environments feature of GitLab and the recently introduced dynamic
environments which can be used for Review Apps.

Review Apps allow you to create a new environment for each of your branches.
This review app is then visible as a link when you visit the merge request
for the branch. That way you are able to see all changes
introduced by the merge request changes, running live.

The example here uses a set of 3 environments:

1. `production`: you trigger deploys to production manually, by clicking the **Production**
   action under the **Pipelines** tab of your project.
1. `staging`: staging is deployed automatically when changes to `master` get
   merged.
1. `review/*`: the review app is created for any other branch that is pushed to GitLab.

## Access the example

This project can be accessed under these addresses:

1. `production`: http://review-apps-openshift.tanukionline.com
1. `staging`: http://review-apps-openshift-staging.tanukionline.com
1. `review` for merge request !1: http://myfeature.tanukionline.com

## Use it for your projects

This is a very simple example, but you can adapt it for your needs and have a
page that is deployed dynamically.

To do that you have to follow these few simple steps.

1. [Optional] Install GitLab on Openshift (version 1.3.1)

    ```
    oc login
    oc new-project gitlab
    wget https://gitlab.com/gitlab-org/omnibus-gitlab/raw/openshift-template/docker/openshift-template.json
    oc create -f openshift-template.json
    ```

1. Add a Secure Variable with your Openshift token, server address, and app domain

    Add a `OPENSHIFT_TOKEN`, `OPENSHIFT_SERVER`, and `OPENSHIFT_DOMAIN` to Secure Variables

You can now start pushing your changes and see them live!
